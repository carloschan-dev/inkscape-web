#
# Copyright 2020, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Count lines as they arrive"""

from django.db import transaction
from django.db.utils import DataError

from .models import (
    GlobalAccess, LanguageAccess, CountryAccess, SystemAccess, BrowserAccess,
    ApplicationAccess, ReleaseAccess, PageAccess, CachingAccess,
)

class NotHandled(ValueError):
    """This result is ignored by this counter"""

class Counters(object):
    COUNTERS = []

    def __init__(self):
        self.counters = [cls() for cls in self.COUNTERS]

    def commit(self, commit=True):
        """Called when you want to empty the cache and save all the objects"""
        report = {}
        count = 0
        for counter in self.counters:
            report[counter.name] = counter.count
            count += counter.commit(commit=commit)
        return report, count

    def process_results(self, results):
        for result in results:
            for counter in self.counters:
                counter.add_entry(result)

class CounterBase(object):
    """A base class for all stat counters"""
    name = property(lambda self: type(self).__name__)
    category = None
    def __init_subclass__(cls):
        if 'Base' not in cls.__name__:
            Counters.COUNTERS.append(cls)

    def __init__(self):
        self.cache = {}
        self.count = 0

    def process_entry(self, **data):
        """Add a category based on the data"""
        return

    @property
    def model(self):
        """The unique fields that define this count"""
        raise NotImplementedError("Please profile an model property")

    @property
    def key_fields(self):
        """The unique fields that define this count"""
        raise NotImplementedError("Please profile an key_fields property")

    def add_entry(self, data):
        """Add this entry based on the data"""
        try:
            ret = self.process_entry(**data)
            if self.category:
                data[self.category] = ret
            elif ret:
                data.update(ret)
        except NotHandled:
            if self.category:
                data[self.category] = None
            return

        created = False
        goc = dict((name, data[key]) for key, name in self.key_fields if data[key])
        if len(goc) != len(self.key_fields):
            return

        key = tuple(goc.values())

        if not key:
            raise ValueError("No key found for adding entry to stats counter")

        if key not in self.cache:
            try:
                obj = self.model.objects.get(**goc)
            except self.model.DoesNotExist:
                # Don't commit anything to the database yet.
                created = True
                obj = self.model(**goc)
            self.cache[key] = obj
        self.count += 1
        self.process_obj(created, self.cache[key], **data)

    @transaction.atomic()
    def commit(self, commit=True):
        """Save all the objects"""
        count = 0
        for obj in self.cache.values():
            if commit:
                try:
                    with transaction.atomic():
                        obj.save()
                except DataError as err:
                    print(f" ! {obj} {err}")
            count += 1
        self.cache = {}
        self.count = 0
        return count

    def process_obj(self, created, obj, **data):
        """Over-ride this method to save your own data"""
        obj.count += 1


class GlobalAccessCounter(CounterBase):
    """Manage adding things to the global-access stats"""
    name = 'glob'
    model = GlobalAccess
    category = 'access_type'
    key_fields = [('date', 'cadence'), ('access_type', 'category')]

    def process_entry(self, path, status, cached, method, **rest):
        """Populate 'access_type' based on other data"""
        if path == 'bot':
            return 'robots'
        if method.upper() == 'HEAD' or status[0] == '3':
            # Redirect and HEAD requests
            return 'unreal'
        if cached:
            return 'cached'
        if status[0] in '45':
            return 'errors'
        if path.split('/')[0] in ('dl', 'media', 'static'):
            return 'direct'
        return 'others'


class LanguageCounter(CounterBase):
    name = 'lang'
    model = LanguageAccess
    key_fields = [('date', 'cadence'), ('lang', 'language')]

    def process_entry(self, access_type, lang, **rest):
        if access_type != 'others' and lang:
            raise NotHandled("Only page requests with a language")
        return None

class CountryCounter(CounterBase):
    name = 'cont'
    model = CountryAccess
    key_fields = [('date', 'cadence'), ('country', 'country')]

    def process_entry(self, access_type, country, **rest):
        if access_type != 'others' and country:
            raise NotHandled("Only page requests with an country")
        return None

class SystemCounter(CounterBase):
    name = 'os'
    model = SystemAccess
    key_fields = [('date', 'cadence'), ('os_name', 'os'), ('os_version', 'version')]

    def process_entry(self, os, access_type, **rest):
        if access_type != 'others' or not os:
            raise NotHandled("Only page requests with an os")
        return {'os_name': os[0], 'os_version': os[1]}

class BrowserCounter(CounterBase):
    name = 'br'
    model = BrowserAccess
    key_fields = [('date', 'cadence'), ('browser_name', 'browser')]

    def process_entry(self, browser, access_type, **rest):
        if access_type != 'others' or not browser:
            raise NotHandled("Only page requests with a browser")
        return {'browser_name': browser[0]}

class ApplicationCounter(CounterBase):
    name = 'app'
    model = ApplicationAccess
    category = 'app'
    key_fields = [('date', 'cadence'), ('app', 'application')]

    def process_entry(self, path, access_type, **rest):
        if access_type != 'others':
            raise NotHandled("Only page requests recorded")
        part, *rest = path.split('/', 1)
        if '★' in path or 'resources' in rest or 'galleries' in rest:
            part = 'gallery'
        if part.startswith('~') or part.startswith('*'):
            part = 'teams'
        if part == 'releases':
            part = 'release' # plural exception
        return {
            'release': 'releases',
            'gallery': 'resources',
            'comments': 'forums',
            'forums': 'forums',
            'polls': 'forums',
            'user': 'person',
            'alerts': 'person',
            'teams': 'person',
            'cals': 'person',
            'news': 'news',
            'cog': 'admin',
            'admin': 'admin',
            'moderation': 'admin',
        }.get(part, 'cmsdoc')


class ReleaseCounter(CounterBase):
    name = 'release'
    model = ReleaseAccess
    key_fields = [('date', 'cadence'), ('release_ver', 'release'), ('release_os', 'os')]

    def process_entry(self, path, **rest):
        if not path.startswith('release/') or not path.endswith('/dl'):
            raise NotHandled("Not a release URL or not a download")
        try:
            (_, version, os, os_ver, *args) = path.split('/')
        except ValueError:
            raise NotHandled("Bad URL / unhandled count")

        return {
            'release_ver': version,
            'release_os': os,
            'release_os_ver': os_ver,
        }

class PageCounter(CounterBase):
    name = 'page'
    model = PageAccess
    key_fields = [('date', 'cadence'), ('page', 'page')]

    def process_entry(self, path, app, **rest):
        if app != 'cmsdoc':
            raise NotHandled("Not a page (doc, cms, etc)")
        if len(path) > 250:
            raise NotHandled("Page too long")
        if path == '':
            path = '/'
        return {
            'page': path,
        }

class CachingCounter(CounterBase):
    name = 'cache'
    model = CachingAccess
    key_fields = [('date', 'cadence'), ('route', 'route'),\
                  ('filename', 'download'), ('cached', 'network')]

    def process_entry(self, path, access_type, status, cached, **rest):
        if not cached or status not in ('200', '206') or access_type != 'cached':
            raise NotHandled("Not a success or cached request")
        if '/' not in path:
            raise NotHandled("Bad path!")
        route, *guff, filename = path.split('/')
        return {'route': route[0].upper(), 'filename': filename}

    def process_obj(self, created, obj, status, size, **other):
        if status == '200':
            obj.complete_count += 1
        else:
            obj.partial_count += 1
        obj.size += size
