
from django.db.models import DateField
from .utils import Week

class WeekField(DateField):
    """A DateField that only saves weeks"""
    def to_python(self, value):
        return Week(super().to_python(value))

class MonthField(DateField):
    """A DateField that only saves months"""
    def to_python(self, value):
        return super().to_python(value).replace(day=1)
