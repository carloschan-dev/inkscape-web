#
# Copyright 2022, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Utils for moderation protections
"""
  
import re
from unidecode import unidecode
from django.forms import ValidationError

def ascii_whitewash(text):
    """ 
    Remove unicode, symbols and multple spaces to give the cleanest
    version of any string.
    """
    # Convert unicode into nearest ascii/romanised equiv
    text = unidecode(text)
    # Remove any remaining symbols and replace with spaces
    text = re.sub(r'[^0-9a-zA-Z]', ' ', text)
    # Replace all multiple spaces with one space
    return re.sub(r'\s+', ' ', text).lower()

class SpamRegex:
    """
    A list of regular expressions which are tested against an input.
    """
    def __init__(self, *args):
        self._patterns = []
        for name, expression in args:
            self._patterns.append((name, re.compile(expression)))

    def is_spam(self, value):
        """Returns the name of the spam filter that tripped this value or False"""
        value = ascii_whitewash(value)
        for name, rex in self._patterns:
            if rex.match(value):
                return name
        return False

    def fail_on_spam(self, value):
        """Raises ValidationError if the spam filter is triggered"""
        if self.is_spam(value):
            # This error is delibrately misleading and untranslated.
            raise ValidationError("Invalid IP Address")

