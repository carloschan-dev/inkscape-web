#
# Copyright 2021, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#

import logging

from django.contrib.sites.models import Site
from django.conf import settings

try:
    from rocketchat.api import RocketChatAPI
    from rocketchat.calls.base import logger
    logger.setLevel(logging.ERROR)
except ImportError:
    RocketChatAPI = None

def get_api():
    if RocketChatAPI is None:
        return None
    try:
        return RocketChatAPI(settings=settings.ROCKET_CHAT_API)
    except Exception:
        pass
    return None

def get_rooms(api):
    return dict([(room['name'], room['id']) for room in api.get_public_rooms()])

def send_message(room_id, msg, api=None):
    """Send a message to a rocket chat room"""
    if api is None:
        api = get_api()
    if api is None:
        return None
    return api.send_message(msg, room_id)

def full_url(url):
    """Return a full site url"""
    domain = Site.objects.get().domain
    url = url.lstrip('/')
    if '://' in url:
        return url
    return f"https://{domain}/{url}"

class DemoAPI:
    def get_public_rooms(self):
        yield {'name': 'team_website', 'id': 'TEAM_WEBSITE_ID'}

    def send_message(self, msg, room_id):
        print(f"Send Message (DEMO) {room_id} - '{msg}'")

    def __getitem__(self, room_name):
        return room_name + '_id'
